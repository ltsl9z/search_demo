import pandas as pd
from utils import GeoTool

def get_recommend(ref_point, search_radius, *args, **kwargs):
    """Search all the places from a circle range and return the recommend places/companies based purely on geo-distance.
    Args:
        ref_point -- tuple, (lat, lon), the pinpoint as centroid search point.
        search_radius -- float, search radius as the name says.
    Return:
        df -- pd.dataframe, recommended result as stored in pandas dataframe.
    """
    # sanity check
    if type(ref_point) != tuple:
            raise TypeError('coordinates need to be supplied in tuple type.')
    if search_radius < 0:
            raise ValueError('search radius needs to be greater than 0.')
    # get GeoTool
    geo_tool = GeoTool()
    # get dummy dataset
    df_tmp = pd.read_csv('dummy_data.csv')
    # calculate distance from ref point for each of the places stored in df.
    df_tmp['distance'] = df_tmp.apply(lambda x : 
    geo_tool.haversine_distance(
        ref_point, (x.latitude, x.longtitude)
        ),
        axis = 1
    )
    # impose search range restraint
    df_tmp = df_tmp[df_tmp.distance < search_radius]
    # rank the places based on distance
    df_tmp['rank'] = df_tmp.distance.rank().astype('int')

    return df_tmp


if __name__ == '__main__':
    ref_point = (-33.867177562043196, 151.20677229372245)
    res = get_recommend(
        ref_point,
        search_radius = 0.5 # 500m
    )
    # display the res table
    print("""**************** display table below ****************""")
    print('\n')
    print(res)
    print('\n')
    print("""**************** display nearest place ****************""")
    print('\n')
    print(res[res['rank'] == 1])
    print('\n')