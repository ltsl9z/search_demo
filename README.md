### Assumption
- we search only based on **geo distance** between user inputted reference point.
### Solution
- We could use [Haversine formula](https://en.wikipedia.org/wiki/Haversine_formula) to calculate distance.
- rank them based on the distance and if distance exceeds search range, exclude them.
- here is the [link](https://www.meridianoutpost.com/resources/etools/calculators/calculator-latitude-longitude-distance.php?) I used for distance testing.
### Code
```shell
pip install -r requirements.txt
python main.py
```