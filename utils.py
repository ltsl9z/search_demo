import numpy as np

class GeoTool(object):
    """Class for managing geo-distance calculation."""
    # define constant used in calculation
    redian = np.pi / 180
    earth_radius = 6371

    def __init__(self):
        pass
    
    @staticmethod
    def haversine_distance(cord_1, cord_2):
        """Calculate great-circle between two points based on haversine formula.
        Args:
            cord_1 - tuple : latitude, longitude of 1st point.
            cord_2 - tuple : latitude, longitude of 2nd point.
        Returns:
            dis - float, great-circle in km between 1st point and 2nd point.
         """
        # sanity check
        if type(cord_1) != tuple or type(cord_2) != tuple:
            raise TypeError('coordinates need to be supplied in tuple type.')
        # retrieve lat, lon params
        lat_1, lon_1 = cord_1
        lat_2, lon_2 = cord_2
        # define const to facilitate calculation
        phi_1 = lat_1 * GeoTool.redian
        phi_2 = lat_2 * GeoTool.redian
        lat_delta = abs(lat_2 - lat_1) * GeoTool.redian / 2
        lon_delta = abs(lon_2 - lon_1) * GeoTool.redian / 2
        # calculation
        a = (np.sin(lat_delta) ** 2) + np.cos(phi_1) * np.cos(phi_2) * (np.sin(lon_delta) ** 2)
        c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
        dis = GeoTool.earth_radius * c
        
        return dis